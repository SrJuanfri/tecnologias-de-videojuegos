﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseLook : MonoBehaviour
{
    public GameObject camerasParent;
    public float hRotationspeed = 100f;
    public float vRotationspeed = 80f;
    public float maxVerticalAngle;
    public float minVerticalAngle;
    public float smoothTime = 0.05f;

    float vCamRotationAngles;
    float hPlayerRotation;
    float CurrentHVelocity;
    float CurrentVVelocity;
    float targetCamEulers;
    Vector3 targetCamRotation;
    
    void Start()
    {
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;

    }

   
    public void handleRotation(float hInput, float vInput)
    {
        float targetPlayerRotation = hInput * hRotationspeed * Time.deltaTime;
        targetCamEulers += vInput * vRotationspeed * Time.deltaTime;

        hPlayerRotation = Mathf.SmoothDamp(hPlayerRotation, targetPlayerRotation, ref CurrentHVelocity, smoothTime);
        transform.Rotate(0f, hPlayerRotation, 0f);

        targetCamEulers = Mathf.Clamp(targetCamEulers, minVerticalAngle, maxVerticalAngle);
        vCamRotationAngles = Mathf.SmoothDamp(vCamRotationAngles, targetCamEulers, ref CurrentVVelocity, smoothTime);
        targetCamRotation.Set(-vCamRotationAngles, 0f, 0f);
        camerasParent.transform.localEulerAngles = targetCamRotation;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NewBehaviourScript : MonoBehaviour
{
    [SerializeField]
    private Vector3 axes;
    public float scaleUnits;

    // Update is called once per frame
    void Update()
    {
        axes = MovimientoTransform1.ClampVector(axes);
        transform.localscale += axes(ScaleUnits * Time.deltaTime);

    }
}
